#!/bin/bash

# Creates a problem for a given problem name
# Each problem contains a .md file named after the problem
# The problem file contains the parts of the problem
# Each problem contains a .psc file named after the problem

mkdir -p $1

echo "# $1" > $1/$1.md

echo "## Description" >> $1/$1.md
echo "### Examples" >> $1/$1.md
echo "## Correct answer" >> $1/$1.md
echo "## Wrong answer" >> $1/$1.md

echo "" > $1/$1.psc

echo '{
  "question": "",
  "description": "",
  "tags": "pseudocode",
  "kind": "Simple",
  "time": 2,
  "active": "TRUE",
  "category": "Logic",
  "correct_answer": "",
  "wrong_answer": [
    "",
    "",
    ""
  ]
}' > $1/$1.json