#!/bin/bash

# Perform sed substitution using the file translations/revert.txt
# Take a parameter and perform the substitution on the file passed as parameter
# The parameter is the name of the file to be modified
sed -f translations/revert.txt $1/$1.out > $1/$1.rev.psc