#!/bin/bash

# Perform sed substitution using the file translations/translations.txt
# Take a parameter and perform the substitution on the file passed as parameter
# The parameter is the name of the file to be modified
sed -f "translations/substitutions.txt" $1/$1.psc > $1/$1.out

