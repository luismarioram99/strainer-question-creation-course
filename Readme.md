# How to create logic questions for strainer

The logic questions for strainer, are meant to test the ability to understand a logical problem and recognize plausible solutions. The participant usually reads a problematic situation or concept, and then is given options in the form of **code** on how to solve this problems. 

The type of logic this questions try to explore is *computational logic*, as opposed to mathematical logic or spatial logic. 

***The questions are meant to be solved in a short period of at most 3 minutes.***

**Here are the steps to write a logic question for Strainer**

## 1. Using this repository

Before you start you should clone this repository which contains a couple of tools that will help you produce a problem file. 

It is recommended to keep all the files for a problem in a folder named after the problem name. Here you will place the `.md` file, the `.psc` file and all the other files that you will need to produce the problem.

Using the script `create_problem.sh` you can create a new problem in this folder. 
To use this script you will need to have access to a linux machine. If you are running Windows it is also recommended to use a [Windows subsystem for linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10) a virtual machine or a docker container.

Similarly, you can use the online tool [repl.it](https://repl.it/languages/bash) to work on this project. For this you can download this repository on your computer and upload the files into the workspace. 

First grant the permission to run the script by executing:
```bash
# Grant permissions to run the script
chmod u+x create_problem.sh
```
Then to create a new problem you can run the following command:

```bash
# Create a new problem
./create_problem.sh <ProblemName>
```
This will create a new folder with the name `ProblemName` in the current directory which contains the problem files.

An example for a .md file is provided in the repository and is named `example.md`. 

## 2. The Idea

The first step to create a question, is to have a concise idea on what kind of problem you want to create.

Formulate a computational logic question, that is, a question that can be solved by using an algorithm.

### Clear 
 The question itself must be easy to explain, as the participants will have at most **three minutes** to solve each of this questions. 

 The question should not be very open to interpretation, as this could cause unnecessary confusion to the participant
  
The options to solve the problem should be easy to understand and require no further explanation.

### Explores a Concept

  Preferably it explores a logical concept that is easy to denote and explain. This can be any concept related to programming, such as:

  - Sorting
  - Searching (Binary Search and Linear Search)
  - Merge Arrays
  - Recursive Functions
  - Well known algorithms
  - Mathematical implementations
  
  Keep in mind that the participant has short time to answer this questions, that short period of time should not be entirely spent in understanding a concept. This concepts should be implied rather than explained. 

### Testable

  It is necessary for these questions to have a definite answer that is not open to interpretation. The easiest way to do this is to have a problem that relies on the execution of a program, so that every distractor yields a wrong answer (or does not execute).   

## 3. The Code

### Program a solution
  Solve the problem in a programming language of your choice. You can use this as a reference to later translate the solution to pseudo code. Make sure you can test that your solution is correct and it works. You can use any method of unit testing or just a simple test.

  It is not necessary for your solution to be the best possible solution, but it should be correct.

  It is important that you don't use any methods or libraries that are not part of the basic set of functions that every programming language provides, such as the `math` library. Avoid fetching to api's, using a complex mathematical library or any syntax that is not completely part of the language, as later you will have to translate the solution to pseudo code.

### Setting up PseInt
  
  For this you will need to install [PseInt](http://pseint.sourceforge.net/) in your computer. 

  The rules are in the file `rules` in this repository and need to be selected from within the PseInt's GUI. 


  After installing PseInt, select the option `Configurar > Opciones del lenguaje` and then press the button `Cargar`. After that, select the rules present in this repository. 

### Pseudocode 

  Take a moment to familiarize with the syntax utilized in this Pseudocode. The language is originally written in spanish, but the syntax is very similar to other programming languages.

  You can read about the syntax [here](http://pseint.sourceforge.net/index.php?page=pseudocodigo.php).

  You will write the solution for the problem in PseInt's pseudocode (or in the case of a *complete the code* problem, an implementation) that works and you can verify that solves the problem. 
  For this, you will use the solution you previously coded in the program language of your choice.
  
  
### Select the code that is relevant to the problem

  Most times, the **relevant** part of the code for the problem will be a snippet of the whole code, rarely will you need to show the participant the whole implementation, and if this implementation is too long, probably you need to specify more clearly the concept you are trying to explore. 

  Because of this, avoid showing the participant things such as the declaration of arrays, the reading of such arrays, the declaration of auxiliary functions, etc. 
  
  In the description of your problem explain the availability of such things, and if you consider it to be of less importance, add a comment in the code so that it is clear. 

### It works

  Make sure your code works by testing it with different inputs. Specially check for execution errors that could occur because of a typo on the program.
  
  This errors are particularly important, because the participant could notice an error like that and assume that the correct answer is the one that has correct syntax, in this case it is not totally the participants fault to get the problem wrong. 

## 4. The Translation

Even though PseInt is a very good programming language and it is very understandable, there are a couple of reasons why we would like to translate this problem to a more fleshed out version in English. 

  - Participants must know English.
  - The translation can be **Language agnostic** 
  - It is easier to understand a sentence than a line of code. 

This is why, after completing the solution in PseInt you will translate the problem to english using a **translation script** this will automatically translate the code to a readable english version, that can later be reverted if needed to PseInt to perform an execution. 

This translation script is written in bash, and uses the GNU tool [sed](https://www.gnu.org/software/sed/manual/sed.html) to translate the spanish keywords. 

To run this bash program, you will need to have access to a linux machine. If you are running Windows it is also recommended to use a [Windows subsystem for linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10) a virtual machine or a docker container. 

To install sed in your windows machine you can run the following commands:

```bash
#update repositories
sudo apt-get update -y

#install sed
sudo apt-get install -y sed
```
  
After this you can run the scripts that perform the translation. 

If the bash files have no permission to run on your machine, you can grant the permission to run by executing

```bash 
#Grant permissions
chmod u+x translate.sh

#Check the permission you have granted
ls -l translate.sh
```

After this you can run the command 
```bash
#translate the psc file you specify
./translate.sh <Problem name>
```
 and this will produce a file `<Problem name>.out` that contains the correct translation to your problem. 

Finally, in case you want to revert the problem to an executable `.psc` file, you can run 
```bash
#Revert the translation
./revert.sh <Problem name>
```

## 5. The Problem File

The objective of all this is to produce an easy to understand `.md` file that contains all things necessary to read the problem. This is because in the website, the problem will be displayed as a [markdown](https://www.markdownguide.org/) snippet. 

In markdown format write a title, description, correct answer and distractors, just like in the example in this repository. 

If you are using **VS Code** you can preview the markdown file using the command `Ctrl + Shift + V`, which will open a window with the preview of the markdown. 

# Test your knowledge

  Now, you will create your own problem. It is not necessary for this test to be an incredibly clever problem, it can be the sum of two numbers, finding the maximum of three numbers, or something very simple. This is just for you to create a problem and make your first `.md` file.

  ## 1. Using the repository

  First you will need to clone the repository, and access it on your computer. After this you will give permissions to the `create_problem` script to run it.

  ```bash
  # Grant permissions to the create_problem script
  chmod u+x create_problem.sh
  ```

  After this you can run the script using a problem name as argument. In this example, the name `MaxOfThree`, is used, as the problem will be to find the maximum of three numbers.

  ```bash
  # Create a problem called MaxOfThree
  ./create_problem.sh MaxOfThree
  ```

  This will create a folder called `MaxOfThree` in the same folder as the `create_problem.sh` script.

  ## 2. The idea
  
  For this example, the idea is to find the maximum of three numbers. But you can use another idea. 

  Here is how the problem would be explained to the participant: 

  > Given the numbers **a, b, c**, the following code will find the maximum of them.
  > What is the code that correctly completes this task?

  ## 2. Solve the problem

  The idea is for you to code the solution in a language that you are familiar with.
  For this example, we will use **Python**.

  ```python
  # Find the maximum of three numbers

  def max_three(a, b, c):
    max = a

    if(max < b):
      max = b

    if(max < c):
      max = c

    return max



  print(max_three(a,b,c)) # 3
  
  ```

  This method `max_three` solves the problem. Even tho it is not necessary to the participant to see that a method is used to solve the problem, it is possible to only show the participant the contents of the method. Like this: 

  ```python
  # Find the maximum of three numbers
  a = 1
  b = 2
  c = 3

  max = a

  if(max < b):
    max = b

  if(max < c):
    max = c

  print(max)
  ```

  This code does the same thing as the method `max_three`, but it is not necessary to the participant to see that the method is used.

  ## 3. Translate to PseInt



  After you have a simple working solution, you will translate it to PseInt. This will be easy, as you can do it line by line. 

  For this, you need to have installed [Pseint](http://pseint.sourceforge.net/index.php?page=pseudocodigo.php). After installing it follow the previous indications to add the rules to the PseInt language.

  In this example we will translate the code in the last section. 
  ```PseInt
  // Find the maximum of three numbers
  a = 1
  b = 2
  c = 3

  max = a

  Si max < b Entonces
    max = b
  FinSi

  Si max < c Entonces
    max = c
  FinSi

  Escribir max

  ```

  As we discussed before, the declarations of the variables a, b and c are not necessary to the participant, so we can remove them.

  ```
  // Find the maximum of three numbers

  max = a

  Si max < b Entonces
    max = b
  FinSi

  Si max < c Entonces
    max = c
  FinSi

  Escribir max

  ```

  ## Translate to English

  Now that the problem is translated to PseInt, we can run the script that translates it to English.
  In this example we will translate the problem `MaxOfThree`
  ```bash
    # Translate the PseInt code to english
    ./translate.sh MaxOfThree
  ```

  This will produce a file `MaxOfThree.out` that contains the english translation of the problem.
  It should look like this:

  ```
  max = a

  If max < b Then
    max = b
  End If

  If max < c Then
    max = c
  End If

  Write max
  ```

  ## 4. Create the problem file

  Now you can copy the contents of the file `MaxOfThree.out` and the description of the problem to the file `MaxOfThree.md`.
  In this example case, we don't need to write examples, as the purpose of max of three is trivial. In some cases you will need to clarify using examples.

  After that delete a part that you consider is key to solve the problem. In this case, both conditionals will be removed. This will leave us with: 

  ```
  max = a
  //Correct code goes here
  ```

  Now the correct answer is: 
  ```
  If max < b Then
    max = b
  End If

  If max < c Then
    max = c
  End If
  ```

  After establishing the correct answer, you will need to add the distractors.
  Add three different distractors to the problem, remember that the reason why the distractors are incorrect must be clear and it must not need further explanation.

  For example: 
```
If a > b Then
  max = a
Otherwise
  max = b
```
```
If a > c Then
  max = a
Otherwise
  max = c
```
  
```
If max > b Then
  max = b
End If

If max > c Then
  max = c
End If
```

In the first distractor, the function does not take into account variable c, in the second distractor, the function does not take into account variable b. In the last distractor, the minimum element is found instead of the maximum.

Done this you have a problem ready to be solved by the participants. 