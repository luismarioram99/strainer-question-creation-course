# Inversions

The inversions of an array is the number of element pairs that are in the wrong order in an unsorted array. In a sorted array, the number of inversions is 0. 

Given an array of length n named k select the code that completes the algorithm to find the number of inversions and store it in the variable counter. 

```
Repeat Starting With i = 0 Until Reaching n-2
  Repeat Starting With j = i+1 Until Reaching n-1			
   //Correct code goes here
  End Repeat Until
End Repeat Until

Write counter;
```

## Correct answer

```
If k[i] > k[j] Then
  contador = contador + 1
End If
```

## Wrong answers

```
If k[i] < k[j] Then
  contador = contador + 1
End If
```

```
If k[i] != k[j] Then
  contador = contador + 1
End If
```

```
If k[i] > k[j] Then
  temp = k[i]
  k[i] = k[j]
  k[j] = temp
End If
```