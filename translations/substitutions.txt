s/<-/is/g

s/Leer/Read/g
s/Escribir/Write/g

s/Dimension/Array/g

s/FinSi/End If/g
s/Fin Si/End If/g
s/SiNo/Otherwise/g
s/Si/If/g
s/Entonces/Then/g

s/FinPara/End Repeat Until/g
s/Fin Para/End Repeat Until/g
s/Para/Repeat Starting With/g
s/Hasta/Until Reaching/g
s/Con Paso/With Increments Of/g

s/FinMientras/End Repeat While/g
s/Fin Mientras/End Repeat While/g
s/Mientras/Repeat While/g
s/Hacer/Do/g

s/FinFuncion/End Function/g
s/Fin Funcion/End Function/g
s/Funcion/Function that returns/g

s/FinAlgoritmo/End Algorithm/g
s/Fin Algoritmo/End Algorithm/g
s/Algoritmo/Start Algorithm Named/g

s/trunc/floor/g

s/ O / Or /g